function draw(){
    var _canvas = document.getElementById('canvas');
    var ctx = _canvas.getContext('2d');  
    
    $('.color input').change(function(){
      R = $('#red').val();
      G = $('#green').val();
      B = $('#blue').val();
      changeColor(R,G,B);
      //取出input中的數值
    });
    
    function changeColor(r,g,b){
      colors = {
        red : r,
        green : g,
        blue : b
      }
      $.each(colors, function(_color, _value) {
        $('#v'+_color).val(_value);
      });
      ctx.strokeStyle = "rgb("+r+","+g+","+b+")" ;
      //將顏色的值寫到ctx.strokeStyle即可
    };
    
    document.getElementById('reset').addEventListener('click', function() {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
    }, false);
    
    document.getElementById('pen').addEventListener('click', function() {
      ctx = _canvas.getContext('2d');
    }, false);
    
    $('.size input').change(function(){
      s = $('#pen_size').val();
      changeSize(s);
      //取出input中的數值
    });
    
    function changeSize(s){
      ctx.lineWidth=s;
      ctx.lineJoin="round"
      ctx.lineCap ="round";
      
    };
    
    var lastX;
    var lastY;
    var strokeWidth=5;
    var mouseX;
    var mouseY;
    var canvasOffset=$("#canvas").offset();
    var offsetX=canvasOffset.left;
    var offsetY=canvasOffset.top;
    var isMouseDown=false;
    
    var drawed_objects = []; //our brain variable
    var isDown;
    var start;
    var end;
    //var canvasEl = document.getElementById("canvas");
    //var draw = canvasEl.getContext("2d");
    //ctx.lineWidth = "2";
    //ctx.strokeStyle = "blue";
    var lastWidth = 0;
    var lastHeight = 0;
    
    var strokeColor = "black";
    var brushSize = 20;
    var brushColor = "#ff0000";
    var points = [];
    
      function handleMouseDown(e){
        if(mode == "pen" || mode == "eraser"){
          mouseX=parseInt(e.clientX-offsetX);
          mouseY=parseInt(e.clientY-offsetY);
      
          // Put your mousedown stuff here
          lastX=mouseX;
          lastY=mouseY;
          isMouseDown=true;
        }
        else if(mode == "rec"){
            isMouseDown = true;
            start = getMousePos(_canvas, e);
            end = getMousePos(_canvas, e);
            lastWidth = 0;
            lastHeight = 0;
            e.preventDefault();
        }
      }
      
      function handleMouseUp(e){
        if(mode == "pen" || mode == "eraser"){
          mouseX=parseInt(e.clientX-offsetX);
          mouseY=parseInt(e.clientY-offsetY);
      
          // Put your mouseup stuff here
          isMouseDown=false;
        }
        else if(mode == "rec"){
          drawed_objects.push({start:start,width:w,height:h});
          isMouseDown = false;
        }
      }
      
      function handleMouseOut(e){
          mouseX=parseInt(e.clientX-offsetX);
          mouseY=parseInt(e.clientY-offsetY);
      
          // Put your mouseOut stuff here
          isMouseDown=false;
      }
      
      function handleMouseMove(e){
        if(mode == "pen" || mode == "eraser"){
          mouseX=parseInt(e.clientX-offsetX);
          mouseY=parseInt(e.clientY-offsetY);
      
          // Put your mousemove stuff here
          if(isMouseDown){
            ctx.beginPath();
            if(mode=="pen"){
              ctx.globalCompositeOperation="source-over";
              ctx.moveTo(lastX,lastY);
              ctx.lineTo(mouseX,mouseY);
              ctx.stroke();     
            }else{
              ctx.globalCompositeOperation="destination-out";
              ctx.arc(lastX,lastY,8,0,Math.PI*2,false);
              ctx.fill();
            }
            lastX=mouseX;
            lastY=mouseY;
          }
        }
        else if(mode == "rec"){
          if (!isMouseDown) return;
            end = getMousePos(_canvas, e);
            h = end.y - start.y;
            w = end.x - start.x;
            ctx.clearRect(start.x-5, start.y-5, lastWidth + 6, lastHeight + 6);
            ctx.beginPath();
            ctx.rect(start.x, start.y, w, h);
            lastWidth = w;
            lastHeight = h;
            ctx.stroke();
            ctx.closePath();
        }
      }
    
    
      //in here we drawing old rectangles again again again..
        //and all time clearing the canvas
        setInterval(function(){
          ctx.clearRect(0,0,draw.canvas.width,draw.canvas.height);
          
          for( let i=0; i <drawed_objects.length; i++ )
          {
            var obj = drawed_objects[i];
            ctx.beginPath();
            ctx.rect(obj.start.x, obj.start.y, obj.width, obj.height);
            ctx.stroke();
            ctx.closePath();
          }
    
        },1000/60);
    
      function getMousePos(canvas, evt) {
            var rect = canvas.getBoundingClientRect();
            return {
                x: Math.floor(evt.clientX - rect.left),
                y: Math.floor(evt.clientY - rect.top)
            };
      }
    
    
    function change_icon(mode){
      if(mode==eraser)
        document.getElementById("canvas").style.cursor = "crosshair";
      else if(mode==pen)
        document.getElementById("canvas").style.cursor = "default";
    }
    
    function downloadCanvas(link, canvasId, filename) {
      link.href = document.getElementById(canvasId).toDataURL();
      link.download = filename;
    }
    document.getElementById('download').addEventListener('click', function() {
      downloadCanvas(this, 'canvas', 'test.png');
    }, false);
    
    function el(id){return document.getElementById(id);} // Get elem by ID
    
    var canvas  = el("canvas");
    var context = canvas.getContext("2d");
    
    function readImage() {
        if ( this.files && this.files[0] ) {
            var FR= new FileReader();
            FR.onload = function(e) {
               var img = new Image();
               img.addEventListener("load", function() {
                 context.drawImage(img, 0, 0);
               });
               img.src = e.target.result;
            };       
            FR.readAsDataURL( this.files[0] );
        }
    }
    
    el("fileUpload").addEventListener("change", readImage, false);
    
    
    $("#canvas").mousedown(function(e){handleMouseDown(e);});
    $("#canvas").mousemove(function(e){handleMouseMove(e);});
    $("#canvas").mouseup(function(e){handleMouseUp(e);});
    $("#canvas").mouseout(function(e){handleMouseOut(e);});
    
    var mode="pen";
    $("#pen").click(function(){ mode="pen"; change_icon(pen)});
    $("#eraser").click(function(){ mode="eraser"; change_icon(eraser)});
    $("#rec").click(function(){ mode="rec";});
    
    
    }
    