# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

Functions:
1. Brush- By clicking the pen-icon, you can draw on the canvas
2. Eraser- By clicking the eraser-icon, you can erase the handwriting on canvas
3. Color Selector- By selecting the ranges of (R,G,B), you can change the color of the pen 
4. Brush size- By selecting the range of size, you can change brush size
5. Text- By typing text in the input area, canvas would show the words in the middle. You can choose font-family, font-color, and 
         font-size by choosing the three options
6. Cursor icon- Cursor would change base on the tool you choose
7. Refresh- Erase the whole canvas by clicking the reset button
8. Brush Shape- By clicking the rectangle icon, you can draw a rectangle
9. Upload image- Choose an image and the image would be display on canvas
10. Download image- Click the download icon, and the image would be saved to your computer

